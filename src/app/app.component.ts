import { Component, EventEmitter } from '@angular/core';
import { Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  public static onLogoutClicked = new EventEmitter();

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private auth: AuthenticationProvider,
    private afAuth: AngularFireAuth,
    private app:App
  ) {
    MyApp.onLogoutClicked.subscribe(() => {
      this.app.getRootNav().setRoot(LoginPage);
    })

    platform.ready().then(() => {
      this.rootPage = auth.isLogedIn() ? HomePage : LoginPage
      afAuth.authState.subscribe(auth => {
        console.log(auth);
      })

      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

