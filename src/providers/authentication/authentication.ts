import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '../../../node_modules/angularfire2/auth';
import { resolveDefinition } from '../../../node_modules/@angular/core/src/view/util';
import { MyApp } from '../../app/app.component';

/*
  Generated class for the AuthenticationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthenticationProvider {

  constructor(
    private afAuth: AngularFireAuth
  ) {

  }

  login(email, password){
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(
        email,
        password
      ).then(auth => {
        localStorage.setItem("auth", JSON.stringify(auth));
        console.log(auth);
        resolve(auth);
      }).catch(err => err);
    })
  }

  isLogedIn(){
    var auth = localStorage.getItem("auth");
    return auth != null && auth != undefined;
  }

  logout(){
    this.afAuth.auth.signOut().then(() => {
      localStorage.removeItem("auth")
      MyApp.onLogoutClicked.emit();
    })
  }

}
