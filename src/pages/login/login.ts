import { Component } from '@angular/core';
import { NavController, NavParams, App, Alert } from 'ionic-angular';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { HomePage } from '../home/home';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private auth: AuthenticationProvider,
    private app: App
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(email, password){
    this.auth.login(email, password).then(auth => {
      console.log(auth);
      this.app.getRootNav().setRoot(HomePage);
    }).catch(err => {
      alert("Login ou senha inválidos");
    });
  }

}
